import React, {Component} from 'react';

export default class VisibilityToggler extends Component {
    constructor(props) {
      super(props);
      this.state = {
        visibility: false
      }
      this.handleVisibility = this.handleVisibility.bind(this);
    }
  
    handleVisibility() {
      this.setState((prevState, props) => {
        return {visibility: !prevState.visibility}
      });
    }
  
    render() {
      return (
        <div>
          <h1>Visibility Toggler</h1>
          <button onClick={this.handleVisibility}>Click Me!</button>
          { this.state.visibility && (
            <div>
              <p>This are some details that you can see.</p>
            </div>
          ) }
        </div>
      );
    }
}