import React, {Component} from 'react';

export default class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = { count: 0 };
    this.handleIncrement = this.handleIncrement.bind(this);
    this.handleDecrement = this.handleDecrement.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }

  componentDidMount() {
    if ( !isNaN(parseInt(localStorage.getItem("count"))) ) {
      this.setState(() => ({
        count: parseInt(localStorage.getItem("count"))
      }));
    }
  }

  componentDidUpdate(prevProps, prevState) {
    localStorage.setItem("count", this.state.count);
  }

  handleIncrement() {
    this.setState((prevState, props) => {
      return {count: prevState.count + 1}
    });
  }

  handleDecrement() {
    this.setState((prevState, props) => {
      return {count: prevState.count - 1}
    });
  }

  handleReset() {
    this.setState(() => {
      return {count: 0}
    });
  }

  render() {
    return (
      <div>
        <h1>Count: {this.state.count}</h1>
        <button onClick={this.handleIncrement}>+1</button>
        <button onClick={this.handleDecrement}>-1</button>
        <button onClick={this.handleReset}>Reset</button>
      </div>
    );
  }
}