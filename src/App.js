import React, {Component} from 'react';
import './App.css';
import AddOption from './components/AddOption';
import Header from './components/Header';
import Action from './components/Action';
import Options from './components/Options';
import OptionModal from './components/OptionModal';

export default class IndecisionApp extends Component {
  state = {
    options: [],
    selectedOption: undefined
  };
  handleDeleteOptions = () => this.setState(() => ({options: []}));
  handleDeleteOption = (optionToRemove) => this.setState((prevState, props) => ({
        options: prevState.options.filter(option => option !== optionToRemove)
      }
    ));
  handlePick = () => {
    let index = Math.floor(Math.random() * this.state.options.length);
    let optionPicked = this.state.options[index];
    this.setState((prevState, props) => ({ selectedOption: optionPicked }));
  }
  handleAddOption = (option) => {
    if (!option) return "Please provide valid option";
    if (this.state.options.includes(option)) return "This option already exists";
    this.setState((prevState, props) => ({options: [...prevState.options, option]}));
  }
  handleClearSelectedOption = () => this.setState((prevState, props) => ({
    selectedOption: undefined
  }));
  componentDidMount() {
    this.setState(() => ({
      options: (localStorage.getItem("options")) ? JSON.parse(localStorage.getItem("options"))
        : []
    }));
  }
  componentDidUpdate(prevProps, prevState) {
    const { options } = this.state;
    if (options.length !== prevState.options.length) {
      localStorage.setItem("options", JSON.stringify(options));
    }
  }
  componentWillUnmount() {
    console.log("component will unmount fires");
  }
  render() {
    return (
      <div>
        <Header subtitle="Put your life in the hands of computer"/>
        <div className="container">
          <Action hasOptions={this.state.options.length} handlePick={this.handlePick}/>
          <div className="widget">
            <Options 
              options={this.state.options} 
              handleDeleteOptions={this.handleDeleteOptions}
              handleDeleteOption={this.handleDeleteOption}
            />
            <AddOption handleAddOption={this.handleAddOption}/>
          </div>
        </div>
        <OptionModal 
          selectedOption={this.state.selectedOption}
          handleClearSelectedOption={this.handleClearSelectedOption}/>
      </div>
    );
  }
}